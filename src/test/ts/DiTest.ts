import * as assert from "assert";
import * as stringify from "json-stringify-safe";
import Container from "@yinz/container/ts/Container";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const Exception = container.getClazz('Exception');


describe('| core.api', function () {
    it('| should assemble the module', function () {
        try {
            // classes
            assert.ok(container.getClazz('ServicesConnectorFactory'));
            assert.ok(container.getClazz('ServicesConnectorController'));
            assert.ok(container.getClazz('RestifyServerPlugins'));
            assert.ok(container.getClazz('RestifyServer'));
            assert.ok(container.getClazz('ReqRspUtils'));
            assert.ok(container.getClazz('CreateResourcesConverter'));
            assert.ok(container.getClazz('LookupResourceConverter'));
            assert.ok(container.getClazz('LookupResourcesConverter'));
            assert.ok(container.getClazz('UpdateResourcesConverter'));
            assert.ok(container.getClazz('DeleteResourcesConverter'));

            // beans
            assert.ok(container.getBean('servicesConnectorFactory'));
            assert.ok(container.getBean('servicesConnectorController'));
            assert.ok(container.getBean('restifyServerPlugins'));
            assert.ok(container.getBean('restifyServer'));
            assert.ok(container.getBean('reqRspUtils'));
            assert.ok(container.getBean('createResourceConverter'));
            assert.ok(container.getBean('lookupResourceConverter'));
            assert.ok(container.getBean('lookupResourcesConverter'));
            assert.ok(container.getBean('updateResourceConverter'));
            assert.ok(container.getBean('deleteResourceConverter'));


        } catch (e) {
            if (e instanceof Exception) {
                assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
            }
            else {
                assert.ok(false, 'exception: ' + e.message + '\n' + e.stack);
            }
        }
    });
});
