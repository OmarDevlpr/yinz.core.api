import { Request, Response } from "restify";
import { InternalRequest, InternalResponse } from "./";
import { ExternalRequest } from "../ts/RestifyServer";

export default interface Converter {    
    buildInternalRequest(extReq: ExternalRequest, intReq: InternalRequest): void;
    buildExternalResponse(intRsp: InternalResponse, extRsp: Response, extReq: ExternalRequest, intReq: InternalRequest): Response;
    buildRejExtRsp(e: any, extRsp: Response, extReq: Request): string;
}