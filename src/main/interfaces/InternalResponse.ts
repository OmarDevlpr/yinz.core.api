import { DefaultServiceRsp } from "@yinz/commons.services/ts/ServiceTemplate";


export interface InternalResponse extends DefaultServiceRsp {
    headers?: any;
    body?: DefaultServiceRsp & { reqDate?: Date };
    reqDate?: Date;
};
