export interface Route {
    method: "get" | "post" | "delete" | "put";
    url: string;
    controller: string;
    action: string;
}