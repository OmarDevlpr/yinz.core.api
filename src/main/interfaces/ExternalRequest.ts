import { Request } from "restify";
import Container from "@yinz/container/ts/Container";
export type ExternalRequest = { container: Container } & Request;