import * as stringify from "json-stringify-safe";
import { Logger, PasswordUtils } from "@yinz/commons";

import { RequestHandler, Response } from "restify";
import ReqRspUtils from "./ReqRspUtils";
import Container from "@yinz/container/ts/Container";
import { ExternalRequest } from "../ts/RestifyServer";

export interface RestifyServerPluginsOptions {
    logger: Logger;
    reqRspUtils: ReqRspUtils;
    container: Container;
}

export default class RestifyServerPlugins {

    private _logger: Logger;
    private _reqRspUtils: ReqRspUtils;
    private _container: Container;

    constructor( options: RestifyServerPluginsOptions) {

        this._logger = options.logger;  
        this._reqRspUtils = options.reqRspUtils;
        this._container = options.container;
    }


    public httpRequestLogger(): RequestHandler {
        let __self__ = this;

        return (req: ExternalRequest, rsp: Response, next) => {

            __self__._logger.info('- Request --------------------------------------------------------------------------');
            __self__._logger.info('New Request   :: Method: %s - URL: %s', req.method, req.url);
            __self__._logger.info('Headers       :: %s', stringify(req.headers, null, 2));

            if (req.body) {
                __self__._logger.info('POST Data     :: %s', stringify(req.body, null, 2));
            }

            if (req.query) {
                __self__._logger.info('Query String  :: %s', stringify(req.query, null, 2));
            }
            __self__._logger.info('-----------------------------------------------------------------------------------');

            next();
        }

    }

    public defaultHeaders(): RequestHandler {

        return (req: ExternalRequest, rsp: Response, next) => {

            rsp.header("Access-Control-Allow-Headers", '' +
                '  Access-Control-Allow-Origin' +
                ', Content-Type' +
                ', Accept' +
                ', Origin' +
                ', Authorization' +
                ', x-req-ref, x-req-date, x-req-user, x-req-lang, x-req-dev-id, x-need-token, x-action' +
                '');

            rsp.header("Access-Control-Expose-Headers", '' +
                'x-token-code' +
                ', x-rsp-reason, x-req-ref, x-req-date, x-req-sys-date, x-rsp-ref, x-rsp-sys-date' +
                '');

            
            next();

        }

    }

    public appendContainer(): RequestHandler {

        return (req: ExternalRequest, rsp: Response, next) => {

            req.container = this._container;
            next();

        }

    }

    public hashPassword(): RequestHandler {

        return async (req: ExternalRequest, rsp: Response, next) => {
            
            req.body = req.body || {};

            let passwordFields = ['password', 'old-password', 'new-password', 'oldPassword', 'newPassword'];
            let reqFieldKeys = Object.keys(req.body);


            for ( let pwdField of passwordFields) {

                if ( reqFieldKeys.includes(pwdField) ) {
                    
                    // get fields 
                    let reqUser  = this._reqRspUtils.lookupHeaderIfAny(req, 'x-req-user') || this._reqRspUtils.lookupHeaderIfAny(req, 'email');

                    if ( !reqUser) {
                        this._logger.warn('will skip password hash as salt was not provided')        
                        continue;                        
                    }

                    let password = this._reqRspUtils.lookupJsonDataIfAny(req, pwdField);

                    this._logger.info(reqUser, password, pwdField)

                    // hash password
                    console.log(password)
                    let hash = await PasswordUtils.hashPassword(password, reqUser);

                    // remove password field
                    this._logger.info('will remove the received `%s` form data...', pwdField);
                    this._reqRspUtils.delJsonFieldIfAny(req, pwdField);

                    // replace digest password
                    this._logger.info('...and replace it by `%sDigest` form data', pwdField);
                    this._reqRspUtils.setJsonFieldIfAny(req, pwdField + 'Digest', hash);
                    
                }

            }

            next();
            
        }


    }
}