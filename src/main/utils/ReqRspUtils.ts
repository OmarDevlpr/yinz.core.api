import * as _ from "lodash";
import { Request, Response } from "restify";
import * as stringify from "json-stringify-safe";
import { Logger, Exception, Asserts } from "@yinz/commons";
import { InternalRequest } from "../interfaces/InternalRequest";

export interface ReqRspUtilsOptions {
    logger: Logger;
}

export default class ReqRspUtils {

    private _logger: Logger;

    constructor(options: ReqRspUtilsOptions) {

        this._logger = options.logger;

    };


    public generateRandomReqRef(): string {
        let min = 100000000000000;
        let max = 199999999999999;
        return '' + (Math.floor(Math.random() * (max - min) + min));
    }


    public setHeaderIfAny(rsp: Response, header: string, value: any): void {
        if (value) {
            rsp.header(header, value);
        }
    }

    public lookupHeaderIfAny(req: Request, name: string, dflt?: string) {
        return req.header(name, dflt);
    }

    /**
     * Lookup request header and throw error if not found
     */
    public lookupHeader(req: Request, name: string, status: number, reason: string): any {
        let data = this.lookupHeaderIfAny(req, name);
        if (!data) {
            throw new Exception(reason || 'ERR_BASE_SRV__LKUP_HDR__MISS_HDR', {
                message: `Header ['${name}'] is missing in the request!`,
                name: name,
                req: req,
                status: status,
            });
        }
        return data;
    }

    public setFormDataIfAny(req: Request, formData: string, value: any): void {
        if (value) {
            req.body[formData] = value;
        }
    }

    public delFormDataIfAny(req: Request, formData: string): void {
        if (req.body[formData]) {
            delete req.body[formData];
        }
    }


    public setJsonFieldIfAny(req: Request, formData: string, value: any): void {
        if (value) {
            req.body[formData] = value;
        }
    }


    public delJsonFieldIfAny(req: Request, field: string): void {
        if (req.body[field]) {
            delete req.body[field];
        }
    }


    public lookupJsonData(req: Request, name: string, status: number, reason: string): any {

        let data = this.lookupJsonDataIfAny(req, name, status, reason, /*assertContentType=*/true);
        if (!data) {
            throw new Exception(reason || 'ERR_BASE_SRV__LKUP_JSON_DTA__MISS_JSON_FIELD', {
                message: `Field [' ${name} '] is missing in the request!`,
                name: name,
                req: req,
                status: status,
            });
        }

        return data;
    }

    public lookupJsonDataIfAny (req: Request, name: string, status?: number, reason?: string, assertContentType: boolean = false): any {

        // make sure that the content-type is correct
        let contentType = req.headers['content-type'];
        if (contentType !== 'application/json') {
            if (!assertContentType) {
                return null;
            }

            throw new Exception(reason || 'ERR_BASE_SRV__LKUP_JSON_DTA__UNEXP_CONT_TYPE', {
                message: `Unexpected content type [' ${contentType} ']! It should be [application/json].`,
                contentType: contentType,
                req: req,
                status: status,
            });
        }

        // make sure that the data has not been received as query parameter
        let data = req.body[name];
        if (data && req.query[name]) {
            throw new Exception(reason || 'ERR_BASE_SRV__LKUP_JSON_DTA__NOT_IN_BODY', {
                message: `Field [' ${name} '] has been sent as query string while it is expected to be in the body!`,
                name: name,
                req: req,
                status: status,
            });
        }

        return data;

    }

    public lookupFormData(req: Request, name: string, status: number, reason: string): any {

        let data = this.lookupFormDataIfAny(req, name, status, reason, /*assertContentType=*/true);
        if (!data) {
            throw new Exception(reason || 'ERR_BASE_SRV__LKUP_FRM_DTA__MISS_FRM_DTA', {
                message: `Form data [' ${name} '] is missing in the request!`,
                name: name,
                req: req,
                status: status,
            });
        }

        return data;
    }

    public lookupFormDataIfAny(req: Request, name: string, status?: number, reason?: string, assertContentType: boolean = false): any {

        // make sure that the content-type is correct
        let contentType = req.headers['content-type'];
        if (contentType !== 'application/x-www-form-urlencoded') {
            if (!assertContentType) {
                return null;
            }

            throw new Exception(reason || 'ERR_BASE_SRV__LKUP_FRM_DTA__UNEXP_CONT_TYPE', {
                message: `Unexpected content type [' ${contentType} ']! It should be [application/x-www-form-urlencoded].`,
                contentType: contentType,
                req: req,
                status: status,
            });
        }

        // make sure that the data has not been received as query parameter
        let data = req.body[name];
        if (data && req.query[name]) {
            throw new Exception(reason || 'ERR_BASE_SRV__LKUP_FRM_DTA__NOT_IN_BODY', {
                message: `Form data [' ${name} '] has been sent as query string while it is expected to be in the body!`,
                name: name,
                req: req,
                status: status,
            });
        }

        return data;

    }

    public lookupParamIfAny(req: Request, name: string, dflt?: string): any {
        return req.params[name] || dflt;
    }

    /**
     * Lookup request param and throw error if not found
     */
    public lookupParam(req: Request, name: string, status: number, reason: string): any {
        let data = this.lookupParamIfAny(req, name);
        if (!data) {
            throw new Exception(reason || 'ERR_BASE_SRV__LKUP_PRM__MISS_PRM', {
                message: `Param ['${name}'] is missing in the request!`,
                name: name,
                req: req,
                status: status,
            });
        }
        return data;
    }


    public lookupAuthorizationHeader(req: Request): string {

        let authorization = this.lookupHeader(req, 'authorization', 499, 'ERR_REQ_REQ_UTL__MISS_AUTH');

        if (!_.startsWith(authorization, 'Bearer ')) {
            throw new Exception('ER_REQ_REQ_UTL__UNEXP_AUTH_PREFIX', {
                message: `'authorization' header [${authorization}] has wrong prefix! It should be 'Bearer '.`,
                authorization: authorization,
                req: req,
                status: 499,
            });
        }
        if (authorization === 'Bearer ') {
            throw new Exception('ER_REQ_REQ_UTL__MISS_TOKEN', {
                message: `Token missing in 'authorization' header .`,
                authorization: authorization,
                req: req,
                status: 499,
            });
        }

        return authorization;
    }


    public lookupTokenCode(req): string {
        return this.lookupAuthorizationHeader(req).replace('Bearer ', '');
    }


    public printRequest(dir: "Inbound" | "Outbound", seq: number, req: Request | any, url?: string): void {


        url = url || (req.isSecure() ? "https" : "http" + '://' + req.headers.host + req.path());
        this._logger.info('- %s request -----------------------------------------------------------------', dir);
        this._logger.info('seq: %d', seq);
        this._logger.info('to: %s %s', req.method, url);
        this._logger.info('headers: %s', stringify(req.headers, null, 2));
        this._logger.info('params: %s', stringify(req.params, null, 2));
        this._logger.info('queries: %s', stringify(req.query, null, 2));
        this._logger.info('payload: %s', stringify(req.body, null, 2));
        this._logger.info('-----------------------------------------------------------------------------------');
    }

    public printResponse(dir: "Inbound" | "Outbound", seq: number, rsp: Response | any, body: any, req: Request | any, url?: string): void {

        url = url || (req.isSecure() ? "https" : "http" + '://' + req.headers.host + req.path());
        this._logger.info('- %s response ----------------------------------------------------------------', dir);
        this._logger.info('seq: %d', seq);
        this._logger.info('of: %s', req.method, url);
        this._logger.info('status: %d', rsp.statusCode);
        this._logger.info('headers: %s', stringify(rsp.getHeaders ? rsp.getHeaders() : undefined, null, 2));
        this._logger.info('payload: %s', stringify(body, null, 2));
        this._logger.info('-----------------------------------------------------------------------------------');
    }


    addEmbed(extReq: Request, fields: string, req: InternalRequest) {

        let body = req.body;

        body.embed = [];

        let fieldsParts = fields.split(/\s*,\s*/);
        for (let field of fieldsParts) {
            body.embed.push(field);
        }

        if (_.isEmpty(body.embed)) {
            delete body.embed;
        }
    }

    addFilter(extReq, filterName, filterValue, req: InternalRequest) {

        let body = req.body || {};
        // body.

        body.filter = body.filter || {};

        // the filter name counld be suffixed with an '_' + comparison operator: ne, lt, lte, gt, gte, in, like
        // let's separate filter name and filter operator
        let filterNameParts = filterName.split(/_/);
        let filterOperator = filterNameParts[1];
        filterName = filterNameParts[0];

        // assert valid comparison operator
        Asserts.contains([undefined, 'ne', 'lt', 'lte', 'gt', 'gte', 'in', 'like'], filterOperator, 'ER_LKUP_RES__BLD_REQ__INV_OP', {
            message: `Invalid operator [${filterOperator}] used with filter [${filterName}]!`,
            filter: filterName,
            operator: filterOperator,
            req: extReq,
        });

        // prepare filter value
        if (filterOperator) {
            let tmp = filterValue;
            filterValue = {};
            filterValue[filterOperator] = tmp;
        }

        // populate filter
        // filter name can contain nested properties
        // -> conversion of name from kabeb case to camel case should be for each nested property
        let normalized: string[] = [];
        for ( let prop of filterName.split(/\./) ) {
            if (prop) {
                normalized.push(_.camelCase(prop));
            }
        }
        filterName = normalized.join('.');
        
        
        const objectBasedFilter = {};
        normalized.reduce( (acc, curr, index ) => {
            if (index === normalized.length - 1) {
                return acc[curr] = filterValue;
            }
            return acc[curr] = {} 
        }, objectBasedFilter)

        this._logger.warn('filterName -->', filterName, filterValue, normalized, objectBasedFilter)
        // filterName = filterName === 'restos.id' ? 'restos.id' : _.camelCase(filterName);
        body.filter = objectBasedFilter;

        if (_.isEmpty(body.filter)) {
            delete body.filter;
        }
    }

    addOrder(extReq, fields, body) {
    
        body.order = [];

        let fieldsParts = fields.split(/\s*,\s*/);
        for (let field of fieldsParts) {
            let fieldParts = field.split(/_/);
            let fieldName = fieldParts[0];
            let sortDir = fieldParts[1];

            // assert valid comparison operator
            Asserts.contains([undefined, 'asc', 'desc'], sortDir, 'ER_LKUP_RES__BLD_REQ__INV_SORT_DIR', {
                message: `Invalid sorting direction [${sortDir}] used with field [${fieldName}]!`,
                field: fieldName,
                sortDir: sortDir,
                req: extReq,
            });

            let orderField = {};
            orderField[_.camelCase(fieldName)] = sortDir || 'asc';

            body.order.push(orderField);
        }

        if (_.isEmpty(body.order)) {
            delete body.order;
        }
    }

}