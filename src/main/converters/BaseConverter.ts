import { InternalRequest, InternalResponse, ExternalRequest, ExternalResponse, Converter } from "../interfaces";
import ReqRspUtils from "../utils/ReqRspUtils";

export interface BaseConverterOptions {
    reqRspUtils: ReqRspUtils;
}

export default class BaseConverter implements Converter {

    protected _reqRspUtils: ReqRspUtils

    constructor(options: BaseConverterOptions) {
        this._reqRspUtils = options.reqRspUtils;
    }

    buildRejExtRsp(e: any, extRsp: ExternalResponse, extReq: ExternalRequest): string {
        
        extRsp.statusCode = e.extra && e.extra.status ? e.extra.status : (e.reason ? 400 : 500);


        this._reqRspUtils.setHeaderIfAny(extRsp, 'x-req-ref', extReq.body.reqRef);
        this._reqRspUtils.setHeaderIfAny(extRsp, 'x-req-date', extReq.body.reqDate);
        this._reqRspUtils.setHeaderIfAny(extRsp, 'x-rsp-sys-date', new Date());
        this._reqRspUtils.setHeaderIfAny(extRsp, 'x-rsp-code', e.reason ? 'D' : 'E');
        this._reqRspUtils.setHeaderIfAny(extRsp, 'x-rsp-reason', e.reason || 'ERR_BASE_SRVC__UNEXP_ERR');


        return e.message;
    }

    buildInternalRequest(extReq: ExternalRequest, intReq: InternalRequest): void {
        intReq.body = intReq.body || {};
        intReq.body.reqUser = this._reqRspUtils.lookupHeaderIfAny(extReq, 'x-req-user');
        intReq.body.reqRef  = this._reqRspUtils.lookupHeaderIfAny(extReq, 'x-req-ref', this._reqRspUtils.generateRandomReqRef());
        intReq.body.reqDate = this._reqRspUtils.lookupHeaderIfAny(extReq, 'x-req-date');
        intReq.body.reqLang = this._reqRspUtils.lookupHeaderIfAny(extReq, 'x-req-lang');
        intReq.body.reqLoc  = this._reqRspUtils.lookupHeaderIfAny(extReq, 'x-real-ip', extReq.connection.remoteAddress); // in case of reverse-proxied requests
    }

    buildExternalResponse(intRsp: InternalResponse, extRsp: ExternalResponse, extReq: ExternalRequest, intReq: InternalRequest): any {
        let extResBody;

        let body = intRsp.body || intRsp || {};

        switch (body.rspCode) {
            case 'A':
                extRsp.statusCode = 200;
                break;

            case 'D':
                extRsp.statusCode = 400;
                extResBody = body.rspMessage;
                break;

            default:
                extRsp.statusCode = 500;
                extResBody = body.rspMessage;
        }

        if (body.rspReason) {
            if (body.rspReason.match(/__TOKEN_NOT_FOUND$/)) {
                extRsp.statusCode = 498;
            }
            else if (body.rspReason.match(/__TOKEN_EXPIRED$/)) {
                extRsp.statusCode = 498;
            }
            else if (body.rspReason.match(/__USER_NOT_FOUND$/)) {
                extRsp.statusCode = 403;
            }
            else if (body.rspReason.match(/__SRV_NOT_PERM$/)) {
                extRsp.statusCode = 402;
            }
        }

        if (extRsp.statusCode !== 200) {
            this._reqRspUtils.setHeaderIfAny(extRsp, 'x-req-ref', body.reqRef);
            this._reqRspUtils.setHeaderIfAny(extRsp, 'x-req-date', body.reqDate);
            this._reqRspUtils.setHeaderIfAny(extRsp, 'x-req-sys-date', body.reqSysDate);
            this._reqRspUtils.setHeaderIfAny(extRsp, 'x-rsp-ref', body.rspRef);
            this._reqRspUtils.setHeaderIfAny(extRsp, 'x-rsp-sys-date', body.rspSysDate);
            this._reqRspUtils.setHeaderIfAny(extRsp, 'x-rsp-code', body.rspCode);
            this._reqRspUtils.setHeaderIfAny(extRsp, 'x-rsp-reason', body.rspReason);
        }

        return extResBody;
    }

}