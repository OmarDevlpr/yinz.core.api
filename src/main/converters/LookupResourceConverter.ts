import BaseConverter, { BaseConverterOptions } from "./BaseConverter";
import { InternalRequest, InternalResponse, ExternalResponse, ExternalRequest } from "../interfaces";

export default class LookupResourceConverter extends BaseConverter {

    constructor(options: BaseConverterOptions) {
        super(options);
    }

    buildInternalRequest(extReq: ExternalRequest, intReq: InternalRequest): void {

        // build base properties
        extReq.body = extReq.body || {}; // in case of GET, the body is undefined.
        super.buildInternalRequest(extReq, intReq);


        // build own properties
        // 1. token code
        intReq.body.tokenCode = this._reqRspUtils.lookupTokenCode(extReq);

        // 2. request user
        intReq.body.reqUser = this._reqRspUtils.lookupHeader(extReq, 'x-req-user', 400, 'ER_LKUP_RES__BLD_REQ__MISS_USER');
        // intReq.body.reqUser = "__super_user__"

        // 3. resource id
        intReq.body.id = Number.parseInt(this._reqRspUtils.lookupParam(extReq, 'id', 400, 'ER_LKUP_RES__BLD_REQ__MISS_RES_ID'));

        // 4. _embed special field
        for (let queryName in extReq.query) {
            if (queryName === '_embed') {
                this._reqRspUtils.addEmbed(extReq, extReq.query[queryName], intReq.body);
            }
        }
    }

    buildExternalResponse(intRsp: InternalResponse, extRsp: ExternalResponse, extReq: ExternalRequest, intReq: InternalRequest): any {

        // build base properties
        let extResBody = super.buildExternalResponse(intRsp, extRsp, extReq, intReq);


        // build own properties


        // build body
        let body: any = intRsp.body || intRsp || {};
        if (body && body.rspCode === 'A') {
            extResBody = body.resource;
        }


        return extResBody;
    }

}