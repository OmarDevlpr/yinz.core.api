import BaseConverter, { BaseConverterOptions } from "./BaseConverter";
import { InternalRequest, InternalResponse, ExternalResponse, ExternalRequest } from "../interfaces";
import * as _ from "lodash";
import { Asserts } from "@yinz/commons";

export default class UpdateResourcesConverter extends BaseConverter {

    constructor(options: BaseConverterOptions) {
        super(options);
    }

    buildInternalRequest(extReq: ExternalRequest, intReq: InternalRequest): void {

        // build base properties
        extReq.body = extReq.body || {}; // in case of GET, the body is undefined.
        super.buildInternalRequest(extReq, intReq);


        // build own properties
        // 1. token code
        intReq.body.tokenCode = this._reqRspUtils.lookupTokenCode(extReq);

        // 2. request user
        intReq.body.reqUser = this._reqRspUtils.lookupHeader(extReq, 'x-req-user', 400, 'ER_CRE_RES__BLD_REQ__MISS_USER');

        // 3. resource
        Asserts.isTruthy(extReq.body, 'ER_UPD_RES__BLD_REQ__MISS_BDY', {
            message: `I\'m expecting a \`resource\` object in the body!`,
            body: extReq.body,
            req: extReq,
            status: 400,
        });
        
        Asserts.isTruthy(_.isPlainObject(extReq.body), 'ER_UPD_RES__BLD_REQ__INV_BDY', {
            message: `I\'m expecting a \`resource\` object in the body but received [${extReq.body}] instead!`,
            body: extReq.body,
            req: extReq,
            status: 400,
        });

        // 4 resource id
        intReq.body.id = Number.parseInt(this._reqRspUtils.lookupParam(extReq, 'id', 400, 'ER_LKUP_RES__BLD_REQ__MISS_RES_ID'));

        intReq.body.resource = extReq.body;
    }

    buildExternalResponse(intRsp: InternalResponse, extRsp: ExternalResponse, extReq: ExternalRequest, intReq: InternalRequest): any {
        // build base properties
        let extResBody = super.buildExternalResponse(intRsp, extRsp, extReq, intReq);


        // build own properties


        // build body
        let body: any = intRsp.body || intRsp || {};
        if (body && body.rspCode === 'A') {
            extResBody = body.resource;
        }


        return extResBody;
    }

}