import BaseConverter, { BaseConverterOptions } from "./BaseConverter";
import { InternalRequest, InternalResponse, ExternalResponse, ExternalRequest } from "../interfaces";
import { Exception } from "@yinz/commons";

export default class LookupResourcesConverter extends BaseConverter {

    constructor(options: BaseConverterOptions) {
        super(options);
    }

    buildInternalRequest(extReq: ExternalRequest, intReq: InternalRequest): void {
        // build base properties
        extReq.body = extReq.body || {}; // in case of GET, the body is undefined.
        super.buildInternalRequest(extReq, intReq);


        // build own properties
        // 1. token code
        intReq.body.tokenCode = this._reqRspUtils.lookupTokenCode(extReq);

        // 2. request user
        intReq.body.reqUser = this._reqRspUtils.lookupHeader(extReq, 'x-req-user', 400, 'ER_LKUP_RES__BLD_REQ__MISS_USER');        
        
        // 3. filter, order, embed
        for (let queryName in extReq.query) {
            // query names that doesn't start with `_` are resource fields names
            if (queryName.charAt(0) !== '_') {
                this._reqRspUtils.addFilter(extReq, queryName, extReq.query[queryName], intReq);
            }

            // _sort special field
            else if (queryName === '_sort') {
                this._reqRspUtils.addOrder(extReq, extReq.query[queryName], intReq);
            }

            // _embed special field
            else if (queryName === '_embed') {
                this._reqRspUtils.addEmbed(extReq, extReq.query[queryName], intReq);
            }

            else if (queryName === '_fields' ) {
              intReq.body.fileds = extReq.query._fields.split(/\s*,\s*/);
            }

            else {
                throw new Exception('ER_LKUP_RES__BLD_REQ__INV_META_DATA', {
                    message: `Invalid meta data [${queryName}]! Should be one of: [_sort].`,
                    metaData: queryName,
                    req: extReq,
                });
            }
        }

    }

    buildExternalResponse(intRsp: InternalResponse, extRsp: ExternalResponse, extReq: ExternalRequest, intReq: InternalRequest): any {
        // build base properties
        let extResBody = super.buildExternalResponse(intRsp, extRsp, extReq, intReq);
        
        // build own properties

        // build body
        let body: any = intRsp.body || intRsp || {};
        if (body && body.rspCode === 'A') {
            extResBody = body.resources;
        }


        return extResBody;
    }

}