// modules used localy when testing
export const LOCAL_MODULES = [
    // 00 core
    '@yinz/commons/ts',
    '@yinz/commons.data/ts',
    '@yinz/access.data/ts',

    // 10 commons
    '@yinz/commons.services/ts',

    // 20 resources
    '@yinz/commons.resources/ts',

    // 30 mom
    '@yinz/mom.client/ts',

    // 40 itself
    process.cwd() + '/dist/main/ts'    
]

// modules used when publishing the module
export const PUBLISHED_MODULES = [
    // 00 core
    '@yinz/commons/ts',
    '@yinz/commons.data/ts',
    '@yinz/access.data/ts',

    // 10 commons
    '@yinz/commons.services/ts',

    // 20 resources
    '@yinz/commons.resources/ts',

    // 30 mom
    '@yinz/mom.client/ts',

    // 40 itself
    // process.cwd() + '/dist/main/ts'
    '@yinz/core.api/ts'
]