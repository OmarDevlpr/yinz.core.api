// yinz
import Container from "@yinz/container/ts/Container";
import { Logger, Asserts } from "@yinz/commons";

// int deps
import RestifyServer from "./RestifyServer";
import { Route } from "../interfaces";
import { LOCAL_MODULES, PUBLISHED_MODULES} from './modules'

// libs
import * as fs from "fs";
import * as _ from "lodash";
import * as yaml from "js-yaml";



const validateRoutes = async (container: Container, routesParams: any, options?: any) => {


    options = options || {};
    const SUPPORTED_METHODS = ['get', 'post', 'delete', 'put', 'GET', 'POST', 'DELETE', 'PUT']

    // routes.<<route>>
    Asserts.isTruthy(_.isPlainObject(routesParams.routes), 'ER_YINZ_API__VAL_ROUTES__INV_ROUTES', {
        message: `routes is missing in the routes config file! It should be an object with at least one route.`
    });
    Asserts.isFalsy(_.isEmpty(routesParams.routes), 'ER_YINZ_API__VAL_ROUTES__EMPTY_ROUTES', {
        message: `routes is missing in the routes config file! It should be an object with at least one route.`
    });

    for ( let route in routesParams.routes) {

        let currentRoute = routesParams.routes[route];

        let controller = currentRoute['controller'];
        let method     = currentRoute['method'];
        let action     = currentRoute['action'];
        let url        = currentRoute['url'];
        
        
        // 1. validate method --> routes.<<route>>.method
        Asserts.isTruthy(SUPPORTED_METHODS.includes(method), 'ER_YINZ_API__VAL_ROUTES__INV_METHOD', {
            message: `method [${method}] for route [${route}] is invalid! It should be on of [${SUPPORTED_METHODS}].`
        });

        // 2. validate url --> routes.<<route>>.url
        Asserts.isNonEmptyString(url, 'ER_YINZ_API__VAL_ROUTES__INV_URL', {
            message: `url [${url}] for route [${route}] is invalid! It should be a non-empty string.`
        });
     
        // 3. validate --> routes.<<route>>.controller
        Asserts.isNonEmptyString(controller, 'ER_YINZ_API__VAL_ROUTES__INV_CTRLER', {
            message: `controller [${controller}] for route [${route}] is invalid! It should be a non-empty string.`
        });

        const controllerBean = container.getBean(_.lowerFirst(controller)) as any;
        Asserts.isTruthy(controllerBean, 'ER_YINZ_API__VAL_ROUTES__TRLER_NOT_FOUND', {
            message: `controller [${controller}] for route [${route}] not found! Make sure it is registered in the assembly.`
        })

        // 4. validate action --> routes.<<route>>.action
        Asserts.isNonEmptyString(action, 'ER_YINZ_API__VAL_ROUTES__INV_ACTION', {
            message: `action [${action}] for route [${route}] is invalid! It should be a non-empty string.`
        });

        Asserts.isTruthy(_.isFunction(controllerBean[action]), 'ER_YINZ_API__VAL_ROUTES__ACTION_NOT_FOUND', {
            message: `action [${action}] for route [${route}] not found! Make sure the controller has the action.`
        })


    }
    
}

const validateExposedServices = async (container: Container, exposedServicesParams: any, options?: any) => {

    options = options || {};        
    // exposedServices.<<exposedService>>
    Asserts.isTruthy(_.isObject(exposedServicesParams.exposedServices), 'ER_YINZ_API__VAL_EXPS_SRV__INV_EXPSD_SRVS', {
        message: `exposedServices is missing in the exposedServices config file! It should be an object with at least one service specs.`
    });
    Asserts.isFalsy(_.isEmpty(exposedServicesParams.exposedServices), 'ER_YINZ_API__VAL_EXPS_SRV__EMPTY_EXPSD_SRVS', {
        message: `exposedServices is missing in the exposedServices config file! It should be an object with at least one service specs.`
    });

    for (let exposedService of exposedServicesParams.exposedServices) {

        let service = exposedService[Object.keys(exposedService)[0]];
        let name = service['name']
        let urn = service['urn']

        // 1. validate name --> exposedServices.<<exposedService>>.name
        Asserts.isNonEmptyString(name, 'ER_YINZ_API__VAL_EXPS_SRV__INV_EXPSD_SRVS_NAME', {
            message: `name [${name}] for exposedServices [${Object.keys(exposedService)[0]}] is invalid! It should be a non-empty string.`
        });

        // 2. validate urn --> exposedServices.<<exposedService>>.urn
        Asserts.isNonEmptyString(name, 'ER_YINZ_API__VAL_EXPS_SRV__INV_EXPSD_SRVS_URN', {
            message: `urn [${urn}] for exposedServices [${Object.keys(exposedService)[0]}] is invalid! It should be a non-empty string.`
        });
    }


}

const validateServer = async (container: Container, params: any, options?: any) => {

    options = options || {};

    // server
    Asserts.isTruthy(_.isPlainObject(params.server), 'ER_YINZ_API__VAL_SRVR__INV_SRVR', {
        message: `server is missing in the params config file! It should be an object with server specs.`
    });
    
    let port = params.server['port']

    // 1. validate server --> server.port
    Asserts.isTruthy(_.isNumber(port), 'ER_YINZ_API__VAL_SRVR__INV_SRVR_PORT', {
        message: `port is invalid! It should be a non negative integer.`
    });

    

}

const validateParams = async (container: Container, options?: any) => {

    options = options || {};
    const params = container.getParams();

    // 1. validate routes    
    let routesPath = process.cwd() + '/src/main/config/routes.yml';
    if (fs.existsSync(routesPath)) {
        let routesFile = yaml.safeLoad(fs.readFileSync(routesPath, 'utf8') as string) as any;
        await validateRoutes(container, routesFile, options);
    }
    

    // 2. validate exposed services    
    let exposedServicesPath = process.cwd() + '/src/main/config/exposedServices.yml';
    if (fs.existsSync(exposedServicesPath)) {
        let exposedServicesFile = yaml.safeLoad(fs.readFileSync(exposedServicesPath, 'utf8') as string) as any;
        await validateExposedServices(container, exposedServicesFile, options);
    }
    
    // 3. validate server
    await validateServer(container, params, options)
    
}

const registerConverters = async (logger: Logger, container: Container): Promise<void> => {

    logger.info('About to register converters...')

    let converterRegCount = 0;

    let convertersRootPath = process.cwd() + '/src/main/converters';
    let convertersPath = fs.readdirSync(convertersRootPath);

    for (let converterPath of convertersPath) {

        let converterFileName = converterPath;
        let converterName = converterFileName.split('.')[0];

        if (_.endsWith(converterName, 'Converter') && converterName !== "Converter" && converterName !== "BaseConverter") {

            let converter = await import(convertersRootPath + "/" + converterPath);            
            converter = converter[Object.keys(converter)[0]]            
            let converterBean = new converter({ reqRspUtils: container.getBean('reqRspUtils') });
            logger.info(`registering converter [${converterName}]...`)
            container.setBean(_.lowerFirst(converterName), converterBean);
            converterRegCount++;

        } else {

            logger.warn(`will skip file ${converterName} as it does not follow converters naming pattern!`);

        }

    }

    logger.info('Register %d converters successfully...', converterRegCount)

}

const registerRoutes = async (logger: Logger, restifyServer: RestifyServer, container: Container): Promise<void> => {

    // default routes for crud
    const { routes } = {
        "routes": {
            "process": {
                "method": "post",
                "url": "/:serviceName/process",
                "controller": "ServicesConnectorController",
                "action": "process"
            },
            "create": {
                "method": "post",
                "url": "/:resourceName",
                "controller": "ServicesConnectorController",
                "action": "create"
            },
            "lookupResource": {
                "method": "get",
                "url": "/:resourceName/:id",
                "controller": "ServicesConnectorController",
                "action": "lookupResource"
            },
            "lookupResources": {
                "method": "get",
                "url": "/:resourceName",
                "controller": "ServicesConnectorController",
                "action": "lookupResources"
            },
            "update": {
                "method": "put",
                "url": "/:resourceName/:id",
                "controller": "ServicesConnectorController",
                "action": "update"
            },
            "delete": {
                "method": "delete",
                "url": "/:resourceName/:id",
                "controller": "ServicesConnectorController",
                "action": "delete"
            }
        }
    }

    logger.info('About to register routes...')
    
    for (let routekey in routes) {
        let route = routes[routekey] as Route;        
        let controller = container.getBean(_.lowerFirst(route.controller)) as any;
        let requestHandler = controller[route.action];

        restifyServer.registerRoute(route.method, route.url, requestHandler.bind(controller))
    }

    logger.info(`Registered ${Object.keys(routes) && Object.keys(routes).length || 0} routes successfully...`)
}

export const startup = async (options?: any) => {

    options = options || {};
    const pkg = require(process.cwd() + '/package.json');
    const modules = options.testing ? LOCAL_MODULES : options.modules || PUBLISHED_MODULES;
    const banner = `      
    ----------------------------------------------------------------------------------------------

        Y88b   d88P d8b                              d8888                  
         Y88b d88P  Y8P                             d88888                  
          Y88o88P                                  d88P888                  
           Y888P    888 88888b.  88888888         d88P 888 88888b.  88888b. 
            888     888 888 "88b    d88P         d88P  888 888 "88b 888 "88b
            888     888 888  888   d88P         d88P   888 888  888 888  888
            888     888 888  888  d88P         d8888888888 888 d88P 888 d88P
            888     888 888  888 88888888     d88P     888 88888P"  88888P" 
                                                           888      888     
                                                           888      888     
                                                           888      888 
                                    ${pkg.version}
    ----------------------------------------------------------------------------------------------
    `

    console.info('          --------------------------------------------------------------------------');
    console.info('                           About to assemble the application modules...');
    console.info('          --------------------------------------------------------------------------');

    console.log(modules)

    // assemble container deps 
    const container = new Container({
        paramsFile: 'params.yml',
        modules: modules
    })

    const restifyServer = container.getBean<RestifyServer>('restifyServer');    
    const logger        = container.getBean<Logger>('logger');

    for (let line of banner.split('\n')) {
        logger.info(line);
    }

    // 1. validate params
    await validateParams(container);

    // 2. connect routes to controllers
    await registerRoutes(logger, restifyServer, container);

    // 3. register all converters beans 
    await registerConverters(logger, container);

    // 4. start server    
    restifyServer.start()
}