// yinz
import Container from "@yinz/container/ts/Container";
import { Logger, Asserts } from "@yinz/commons";
import RedisMomClient  from "@yinz/mom.client/ts/RedisMomClient";

// int deps 
import { ExternalRequest, ExternalResponse, Converter, InternalRequest, InternalResponse } from "../interfaces/";
import ReqRspUtils from "../utils/ReqRspUtils";

// libs
import * as util from "util";
import * as yaml from "js-yaml";
import * as fs from "fs";

// types 
type actionType = 'create' | 'delete' | 'update' | 'lookupResource' | 'lookupResources' | 'process';

// seq counter
let intReqCounter = 0;
let extReqCounter = 0;

// class options
export interface ServicesConnectorFactoryOptions {    
    container: Container;
    logger: Logger;
    params: any;
    reqRspUtils: ReqRspUtils;
}

export default class ServicesConnectorFactory {
    
    private exposedServices: string[];    
    private _container: Container;
    private _logger: Logger;
    private _urns: Map<string, string>;
    private _reqRspUtils: ReqRspUtils;

    constructor(options: ServicesConnectorFactoryOptions) {

        this._logger = options.logger;
        this._container = options.container;
        this._reqRspUtils = options.reqRspUtils;
        this._urns = new Map<string, string>();
        this.exposedServices = [];
        
        // register default services
        this.registerDefaultServices()
        this.registerDefaultUrns()

        // register app services (overrides defaults if desired)
        let servicesPath = process.cwd() + '/src/main/config/exposedServices.yml';
        if (fs.existsSync(servicesPath)) {
            let { exposedServices } = yaml.safeLoad(fs.readFileSync(servicesPath, 'utf8') as string) as any;
            exposedServices = exposedServices || [];
            this.exposedServices = exposedServices.map(srv => srv[Object.keys(srv)[0]].name);                        

            this.registerUrns(exposedServices.map(srv => ({ 
                name: srv[Object.keys(srv)[0]].name,
                urn: srv[Object.keys(srv)[0]].urn
            })))
        }

    };    

    private registerDefaultServices(): void {
        this.exposedServices = [
            ...this.exposedServices,            
            "lookupResourceService" ,
            "lookupResourcesService",
            "createResourceService" ,
            "updateResourceService" ,
            "deleteResourceService"
        ];
    }

    private registerDefaultUrns(): void {
        this._urns.set("lookupResourceService", "resource:lookupResourceService");
        this._urns.set("lookupResourcesService", "resource:lookupResourcesService");
        this._urns.set("createResourceService", "resource:createResourceService");
        this._urns.set("updateResourceService", "resource:updateResourceService");
        this._urns.set("deleteResourceService", "resource:deleteResourceService");
    }

    private registerUrns (urnsObj: any[]): void {        
        for ( let srv of urnsObj ) {            
            this._urns.set(srv.name, srv.urn)
        }
    }

    private async lookupConverter(serviceName: string): Promise<Converter> {

        // validate the serviceName
        Asserts.isNonEmptyString(serviceName, 'ERR_SERVICE_CONVERTER__INVALID_SERVICE_NAME', {
            message: util.format('Invalid service name [%s]! It should be a none empty string.', serviceName),
        });

        // make sure the service is exposed by the app
        Asserts.contains(this.exposedServices, serviceName, 'ERR_SERVICE_CONVERTER__SERVICE_NOT_EXPOSED', {
            message: util.format('The service [%s] is not exposed on this app!', serviceName),
            status: 404,
        });

        // require service
        let converterName = serviceName + "Converter";

        converterName = converterName.replace(/Service/gi, '')

        let converter: Converter = this._container.getBean(converterName);

        // assert converter
        Asserts.isTruthy(converter, 'ERR_SERVICE_CONVERTER__CONVERTER_NOT_FOUND', {
            message: util.format('The converter [%s] doesn\'t exist!', converterName),
        });

        return converter;
    }


    private async submit(req: ExternalRequest, rsp: ExternalResponse, action: actionType, serviceName: string, resourceName?: string): Promise<void> {

        this._logger.trace(`Entering submit ${(resourceName || serviceName)} , ${req.query}`)

        let momClient = req.container.getBean<RedisMomClient>('redisMomClient');
        let extReqSeq = extReqCounter++;
        let intReqSeq = intReqCounter++;
        let converter: Converter = null as any;

        // external request variables
        let extReq     = req  ;
        let extRsp     = rsp  ;
        let extRspBody = null as any;           

        // internal request variables
        let intRsp: InternalResponse = null as any;
        let intReq: InternalRequest = { headers: {}, body: {} };


        try {

            this._reqRspUtils.printRequest('Inbound', extReqSeq, extReq);            

            // service converter 
            converter = await this.lookupConverter(serviceName)

            // build internal request
            await converter.buildInternalRequest(extReq, intReq);

            // add resourceName to request if any
            if (resourceName) {
                intReq.body.resourceName = resourceName;
            }

             // the request's urn
            let urn = this._urns.get(resourceName ? action : serviceName) as string;
            Asserts.isTruthy(urn, 'ERR_SRVC_CONN_FACT__SUBMIT__URN_NOT_FOUND', {
                message: `URN for [${resourceName ? action + ':' + resourceName ? action : serviceName : serviceName}] does not exist!`,
                action: action,
                resourceName: resourceName,
                serviceName: serviceName,
                status: 400,
            });


            this._reqRspUtils.printRequest('Outbound', intReqSeq, intReq, urn)

            // send the internal request and wait the response
            let options = {};
            intRsp = await momClient.submit(urn, intReq.body, options);

            this._reqRspUtils.printResponse('Inbound', intReqSeq, intRsp, intRsp, intReq, intReq.headers);

            extRspBody = converter.buildExternalResponse(intRsp, extRsp, extReq, intReq);

        } catch (e) {
            this._logger.warn(e.stack || e.message);
            extRspBody = converter.buildRejExtRsp(e, extRsp, extReq);

        } finally {
            this._reqRspUtils.printResponse('Outbound', extReqSeq, extRsp, extRspBody, extReq);
            this._logger.trace(`--} () -> ${extRspBody}`);
            extRsp.send(extRspBody);
        }
    }


    public async create(req: ExternalRequest, rsp: ExternalResponse) {
        let serviceName = "createResourceService";
        let resourceName = req.params['resourceName'];
        await this.submit(req, rsp, "create", serviceName, resourceName)
    };

    public async update(req: ExternalRequest, rsp: ExternalResponse) {
        let serviceName = "updateResourceService";
        let resourceName = req.params['resourceName'];
        await this.submit(req, rsp, "update", serviceName, resourceName)
    };

    public async delete(req: ExternalRequest, rsp: ExternalResponse) {
        let serviceName = "deleteResourceService";
        let resourceName = req.params['resourceName'];
        await this.submit(req, rsp, "delete", serviceName, resourceName)
    };

    public async lookupResource(req: ExternalRequest, rsp: ExternalResponse) {
        let serviceName = "lookupResourceService";
        let resourceName = req.params['resourceName'];
        await this.submit(req, rsp, "lookupResource", serviceName, resourceName)
    };

    public async lookupResources(req: ExternalRequest, rsp: ExternalResponse) {
        let serviceName = "lookupResourcesService";
        let resourceName = req.params['resourceName'];
        await this.submit(req, rsp, "lookupResources", serviceName, resourceName)
    };

    public async process(req: ExternalRequest, rsp: ExternalResponse) {
        let serviceName = req.params['serviceName'];
        await this.submit(req, rsp, "process", serviceName)
    };


}