import YinzServer from "../interfaces/YinzServer";
import { RequestHandler, Request, Response, Server } from "restify";
import * as restify from "restify";
import Logger from "@yinz/commons/ts/Logger";
import RestifyServerPlugins from "../utils/RestifyServerPlugins";
import Container from "@yinz/container/ts/Container";
// import * as helmet from "helmet";

const corsMiddleware = require('restify-cors-middleware');


const cors = corsMiddleware({
    preflightMaxAge: 5,
    origins: ['*'],
    allowHeaders: [
        'Access-Control-Allow-Origin', 'Content-Type', 'Accept', 'Origin', 'Authorization',
        'x-req-ref', 'x-req-date', 'x-req-user', 'x-req-lang', 'x-req-dev-id', 'x-need-token', 'x-action',

    ],
    exposeHeaders: [
        'x-token-code', 'x-rsp-reason', 'x-req-ref', 'x-req-date', 'x-req-sys-date', 'x-rsp-ref', 'x-rsp-sys-date'
    ]
});

export interface RestifyServerOptions {
    logger: Logger
    serverPlugins: RestifyServerPlugins;
    port: number;
}

export type ExternalRequest = { container: Container } & Request;



export default class RestifyServer implements YinzServer {

    private _restify: Server;
    private _logger: Logger;
    private _serverPlugins: RestifyServerPlugins;
    private _port: number;

    constructor(options: RestifyServerOptions) {

        this._restify = restify.createServer();
        this._port = options.port || 8080;

        this._logger = options.logger;
        this._serverPlugins = options.serverPlugins;

    }

    get(url: string, requestHandler: RequestHandler): void {
        this.registerRoute("get", url, requestHandler);
    }

    post(url: string, requestHandler: RequestHandler): void {
        this.registerRoute("post", url, requestHandler);
    }

    put(url: string, requestHandler: RequestHandler): void {
        this.registerRoute("put", url, requestHandler);
    }

    delete(url: string, requestHandler: RequestHandler): void {
        this.registerRoute("delete", url, requestHandler);
    }


    public registerRoute(method: 'get' | 'post' | 'put' | 'delete',
        url: string,
        requestHandler: RequestHandler,
    ) {
        // console.log('s-->', method)

        let restifyMethod = method === "delete" ? "del" : method

        this._restify[restifyMethod](url, async (req: ExternalRequest, rsp: Response, next) => {

            try {
                await requestHandler(req, rsp, next)
            } catch (e) {
                this._logger.error(e);
                rsp.send(500, e);
            }

        });


    }

    public start() {

        // TODO fix the types to support restify
        // this._restify.use(helmet());        

        // parse body to get post data        
        this._restify.use(restify.plugins.bodyParser());

        // parse request query 
        this._restify.use(restify.plugins.queryParser());

        // cors
        this._restify.pre(cors.preflight);
        this._restify.use(cors.actual);

        // rsp headers
        this._restify.use(this._serverPlugins.defaultHeaders());

        // hash password if any before the request gets logged
        this._restify.use(this._serverPlugins.hashPassword());

        // log request
        this._restify.use(this._serverPlugins.httpRequestLogger());

        // append container to request
        this._restify.use(this._serverPlugins.appendContainer());

        this._restify.listen(this._port);

        this._logger.info('server is listening at port %d...', this._port)

    }

}