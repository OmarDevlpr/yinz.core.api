import Container from "@yinz/container/ts/Container";
import { Logger } from "@yinz/commons";
import RestifyServerPlugins from "../utils/RestifyServerPlugins";
import ReqRspUtils from "../utils/ReqRspUtils";
import RestifyServer from "./RestifyServer";
import ServicesConnectorFactory from "./ServicesConnectorFactory";
import ServicesConnectorController from "../controllers/ServicesConnectorController";
import CreateResourcesConverter from "../converters/CreateResourceConverter";
import LookupResourceConverter from "../converters/LookupResourceConverter";
import LookupResourcesConverter from "../converters/LookupResourcesConverter";
import UpdateResourcesConverter from "../converters/UpdateResourceConverter";
import DeleteResourcesConverter from "../converters/DeleteResourceConverter";



const registerBeans = (container: Container) => {

    let logger = container.getBean<Logger>('logger');

    // utils 
    let reqRspUtils = new ReqRspUtils({ logger })
    let restifyServerPlugins = new RestifyServerPlugins({ logger, reqRspUtils, container })
    let servicesConnectorFactory = new ServicesConnectorFactory({
        params: container.getParams(),
        logger,
        container,
        reqRspUtils
    })

    // controlers
    let servicesConnectorController = new ServicesConnectorController(servicesConnectorFactory);

    // server
    let restifyServer = new RestifyServer({
        logger,
        serverPlugins: restifyServerPlugins,
        port: container.getParam('server') && container.getParam('server').port
    })

    // converters
    const createResourceConverter  = new CreateResourcesConverter({reqRspUtils});
    const lookupResourceConverter  = new LookupResourceConverter({reqRspUtils});
    const lookupResourcesConverter = new LookupResourcesConverter({reqRspUtils});
    const updateResourceConverter  = new UpdateResourcesConverter({reqRspUtils});
    const deleteResourceConverter  = new DeleteResourcesConverter({reqRspUtils});    

    container.setBean('servicesConnectorFactory', servicesConnectorFactory);
    container.setBean('servicesConnectorController', servicesConnectorController);
    container.setBean('restifyServerPlugins', restifyServerPlugins);
    container.setBean('restifyServer', restifyServer);
    container.setBean('reqRspUtils', reqRspUtils);
    container.setBean('createResourceConverter', createResourceConverter);
    container.setBean('lookupResourceConverter', lookupResourceConverter);
    container.setBean('lookupResourcesConverter', lookupResourcesConverter);
    container.setBean('updateResourceConverter', updateResourceConverter);
    container.setBean('deleteResourceConverter', deleteResourceConverter);

};

const registerClazzes = (container: Container) => {

    container.setClazz('ServicesConnectorFactory', ServicesConnectorFactory);
    container.setClazz('ServicesConnectorController', ServicesConnectorController);
    container.setClazz('RestifyServerPlugins', RestifyServerPlugins);
    container.setClazz('RestifyServer', RestifyServer);
    container.setClazz('ReqRspUtils', ReqRspUtils);
    container.setClazz('CreateResourcesConverter', CreateResourcesConverter);
    container.setClazz('LookupResourceConverter', LookupResourceConverter);
    container.setClazz('LookupResourcesConverter', LookupResourcesConverter);
    container.setClazz('UpdateResourcesConverter', UpdateResourcesConverter);
    container.setClazz('DeleteResourcesConverter', DeleteResourcesConverter);

};


export {
    registerBeans,
    registerClazzes
};