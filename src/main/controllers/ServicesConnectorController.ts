import ServicesConnectorFactory from "../ts/ServicesConnectorFactory";
import { Response } from "restify";
import { ExternalRequest } from "main/ts/RestifyServer";

export default class ServicesConnectorController {

    constructor(private _servicesConnectorFactory: ServicesConnectorFactory) {

    }

    public async create(req: ExternalRequest, rsp: Response): Promise<void> {
        this._servicesConnectorFactory.create(req, rsp)
    }
    public async delete(req: ExternalRequest, rsp: Response): Promise<void> {
        this._servicesConnectorFactory.delete(req, rsp)
    }
    public async lookupResource(req: ExternalRequest, rsp: Response): Promise<void> {
        this._servicesConnectorFactory.lookupResource(req, rsp)
    }
    public async lookupResources(req: ExternalRequest, rsp: Response): Promise<void> {
        this._servicesConnectorFactory.lookupResources(req, rsp)
    }
    public async process(req: ExternalRequest, rsp: Response): Promise<void> {
        this._servicesConnectorFactory.process(req, rsp)
    }
    public async update(req: ExternalRequest, rsp: Response): Promise<void> {
        this._servicesConnectorFactory.update(req, rsp)
    }

} 